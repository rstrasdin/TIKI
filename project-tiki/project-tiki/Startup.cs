﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(project_tiki.Startup))]
namespace project_tiki
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
