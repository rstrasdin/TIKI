﻿// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function sendNotification(title, options) {
    // Memoize based on feature detection.
    if ("Notification" in window) {
        sendNotification = function (title, options) {
            return new Notification(title, options);
        };
    } else if ("mozNotification" in navigator) {
        sendNotification = function (title, options) {
            // Gecko < 22
            return navigator.mozNotification
                     .createNotification(title, options.body, options.icon)
                     .show();
        };
    } else {
        sendNotification = function (title, options) {
            alert(title + ": " + options.body);
        };
    }
    return sendNotification(title, options);
}

var option = {
    icon: 'https://www.gravatar.com/avatar/f958a05a51bb51a02a2e7e6b1934064f?d=identicon',
    body: '{{TYPE}} {{NAME}}'
};

sendNotification("{{USERNAME}} updated {{TYPE}}", option);