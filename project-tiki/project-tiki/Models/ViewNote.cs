﻿using project_tiki.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_tiki.Models
{
    public class ViewNote
    {
        public ViewNote(Note baseNote, List<AspNetUser> members, DateTime lastEdited, List<Tag> tags)
        {
            this.Title = baseNote.Title;
            this.Members = members;
            this.LastEdited = lastEdited;
            this.Content = baseNote.Content;
            this.NoteID = baseNote.Id;
            this.Background = baseNote.Background_Color;
            this.Color = baseNote.Font_Color;
            this.FontSource = baseNote.Font_Url;
            this.Tags = tags;
        }

        public String Title { get; set; }
        public List<AspNetUser> Members { get; set; }
        public DateTime LastEdited { get; set; }
        public String Content { get; set; }
        public int NoteID { get; set; }
        public String Background { get; set; }
        public String Color { get; set; }
        public String FontSource { get; set; }
        public List<Tag> Tags { get; set; }
    }
}