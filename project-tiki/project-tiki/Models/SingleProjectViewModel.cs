﻿using project_tiki.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_tiki.Models
{
    public class SingleProjectViewModel
    {
        public SingleProjectViewModel(ViewProject theProject, List<AspNetUser> members, List<TaskList> taskLists)
        {
            this.TheProject = theProject;
            this.Members = members;
            this.TaskLists = taskLists;
        }

        public ViewProject TheProject { get; set; }
        public List<AspNetUser> Members { get; set; }
        public List<TaskList> TaskLists { get; set; }
    }
}