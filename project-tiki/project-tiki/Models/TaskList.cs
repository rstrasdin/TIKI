﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_tiki.Models
{
    public class TaskList
    {

        public TaskList(List<SingleTask> tasks, int id, String name)
        {
            this.ID = id;
            this.Name = name;
            this.Tasks = tasks;
        }

        public int ID { get; set; }

        public String Name { get; set; }
        public List<SingleTask> Tasks { get; set; }
    }
}