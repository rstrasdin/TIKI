﻿using project_tiki.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_tiki.Models
{
    public class ViewProject
    {

        public ViewProject(Project baseProject, List<AspNetUser> members, DateTime lastEdited)
        {
            this.Title = baseProject.Name;
            this.Members = members;
            this.LastEdited = lastEdited;
            this.ProjectID = baseProject.Id;
            this.Background = baseProject.Background_Color;
            this.Color = baseProject.Font_Color;
            this.FontSource = baseProject.Font_Url;
            this.DateCreated = baseProject.Date_Created;
        }

        public String Title { get; set; }
        public List<AspNetUser> Members { get; set; }
        public DateTime LastEdited { get; set; }
        public DateTime DateCreated { get; set; }
        public int ProjectID { get; set; }
        public String Background { get; set; }
        public String Color { get; set; }
        public String FontSource { get; set; }

        public override string ToString()
        {
            String tempStr = "";
            for(int i = 0; i < Members.Count; i++)
            {
                tempStr += Members.ElementAt(i).UserName;
                if( i + 1 < Members.Count)
                {
                    tempStr += ", ";
                }
            }
            return tempStr;
        }
    }
}