﻿using project_tiki.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_tiki.Models
{
    public class NoteViewEditViewModel
    {
        public NoteViewEditViewModel(ViewNote theNote, List<AspNetUser> members)
        {
            this.TheNote = theNote;
            this.Members = members;
        }
        public ViewNote TheNote { get; set; }
        public List<AspNetUser> Members { get; set; }
    }
}