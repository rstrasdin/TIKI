﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_tiki.Models
{
    public class UserLandingViewModel
    {
        public UserLandingViewModel(List<ViewProject> userProjects, List<ViewNote> userNotes)
        {
            this.UserProjects = userProjects;
            this.UserNotes = userNotes;
        }
        public List<ViewProject> UserProjects { get; set; }
        public List<ViewNote> UserNotes { get; set; }
    }
}