﻿using project_tiki.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project_tiki.Models
{
    public class SingleTask
    {
        public SingleTask(List<AspNetUser> members, Note baseNote, DateTime lastEdited)
        {
            this.Members = members;
            this.Content = baseNote.Content;
            this.Title = baseNote.Title;
            this.TaskID = baseNote.Id;
            
            //Will update when database is updated
            this.Color = baseNote.Font_Color;
            this.FontSource = baseNote.Font_Url;
            if(baseNote.Date_Due != null)
            {
                this.DueDate = (DateTime)baseNote.Date_Due;
            }else
            {
                this.DueDate = DateTime.Now.AddDays(1);
            }
            this.DateCreated = (DateTime)baseNote.Date_Created;
            this.LastEdited = lastEdited;
        }
        public List<AspNetUser> Members { get; set; }
        public String Content { get; set; }
        public String Title { get; set; }
        public int TaskID { get; set; }
        public String Background { get; set; }
        public String Color { get; set; }
        public String FontSource { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastEdited { get; set; }
    }
}