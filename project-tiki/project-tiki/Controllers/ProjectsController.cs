﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using project_tiki.Entities;

namespace project_tiki.Controllers
{
    public class ProjectsController : Controller
    {
        private TikiEntities db = new TikiEntities();

        // GET: Projects
        public ActionResult Index()
        {
            return View(db.Projects.ToList());
        }

        // GET: Projects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // GET: Projects/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Projects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Date_Created,Date_Due,Color")] Project project)
        {
            if (ModelState.IsValid)
            {
                db.Projects.Add(project);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(project);
        }

        // GET: Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Date_Created,Date_Due,Color")] Project project)
        {
            if (ModelState.IsValid)
            {
                db.Entry(project).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(project);
        }

        // GET: Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project project = db.Projects.Find(id);


            //Archive and Log Notes Related to the Project
            List<ProjectNote> projectNotes = db.ProjectNotes.Where(pn => pn.ProjectId == project.Id).ToList();
            List<Note> notes = db.Notes.ToList();
            foreach(Note note in notes)
            {
                foreach (ProjectNote projectNote in projectNotes)
                {
                    if (note.Id.Equals(projectNote.NoteId))
                    {
                        NoteArchive noteArchive = new NoteArchive { Title = note.Title, Content = note.Content, Date_Created = note.Date_Created, Date_Due = DateTime.Now.AddDays(30)};
                        db.NoteArchives.Add(noteArchive);
                        Log log = new Log { Date_Edited = DateTime.Now, Editor = User.Identity.GetUserId(), StoredId = "Z" + noteArchive.Id, Details = note.Id + " was and is now Z" + noteArchive.Id };
                        //Making NoteTags refer to the NoteArchive.StoredId
                        foreach(NoteTag tag in db.NoteTags.Where(t => t.StoredId == "N" + note.Id))
                        {
                            tag.StoredId = log.StoredId;
                        }
                        
                        db.Logs.Add(log);
                        db.Notes.Remove(note);
                        db.ProjectNotes.Remove(projectNote);
                    }
                }
            }
            //Archive and Log Project
            ProjectArchive projectArchive = new ProjectArchive { Name = project.Name, Date_Created = project.Date_Created, Date_Due = DateTime.Now.AddDays(30) };
            db.ProjectArchives.Add(projectArchive);
            db.Logs.Add(new Log { Date_Edited = DateTime.Now, Editor = User.Identity.GetUserId(), StoredId = "Q" + projectArchive.Id, Details = project.Id + " was and is now Q" + projectArchive.Id });
            db.TeamProjects.Remove(db.TeamProjects.Where(p => p.ProjectId == project.Id).FirstOrDefault());
            db.Projects.Remove(project);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
