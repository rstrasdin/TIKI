﻿using project_tiki.Entities;
using Microsoft.AspNet.Identity;
using project_tiki.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Globalization;

namespace project_tiki.Controllers
{
    public class SingleProjectController : Controller
    {
        private TikiEntities db = new TikiEntities();
        // GET: SingleProject
        public ActionResult Index(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Project proj = db.Projects.Find(id);
            if (proj == null)
            {
                return HttpNotFound();
            }
            if (!userHasAccess((int)id))
            {
                //throw new UnauthorizedAccessException("Sorry, you do not have access to that team | We'll probably want to redirect them maybe?");
                return RedirectToAction("Index", "UserLanding");
            }

            int curTeamID = db.TeamProjects.Where(tp => tp.ProjectId == proj.Id).FirstOrDefault().TeamId;
            //gets all teamMembers on that team
            List<TeamMember> teamMembers = db.TeamMembers.Where(tm => tm.TeamId == curTeamID).ToList();
            List<AspNetUser> theMembers = new List<AspNetUser>();
            foreach (TeamMember tm in teamMembers)
            {
                var memID = tm.MemberId;
                //adds all AspNetUser counterparts to teamMembers
                theMembers.AddRange(db.AspNetUsers.Where(p => p.Id.Equals(memID)));
            }
            DateTime lastEdited = proj.Date_Created;
            String tempStoredID = "P" + proj.Id;
            var lastEdit = db.Logs.Where(l => l.StoredId.Equals(tempStoredID)).ToList();
            for (int j = 0; j < lastEdit.Count; j++)
            {
                if (j == lastEdit.Count - 1)
                {
                    lastEdited = lastEdit.ElementAt(j).Date_Edited;
                }
            }
            ViewProject tempViewProject = new ViewProject(proj, theMembers, lastEdited);
            List<TaskList> projectTasks = new List<TaskList>();

            List<ProjectNote> pns = db.ProjectNotes.Where(pn => pn.ProjectId == proj.Id).ToList();

            Dictionary<Tag, List<SingleTask>> TagToList = new Dictionary<Tag, List<SingleTask>>();

            foreach (ProjectNote pn in pns)
            {
                Note tempNote = pn.Note;
                SingleTask tempTask = new SingleTask(theMembers, tempNote, lastEdited);
                String tempStore = "N" + tempNote.Id;
                List<NoteTag> tags = db.NoteTags.Where(nt => nt.StoredId.Equals(tempStore)).ToList();
                foreach (NoteTag nt in tags)
                {
                    Tag tempTag = nt.Tag;
                    if (TagToList.ContainsKey(tempTag))
                    {
                        List<SingleTask> oldTasks;
                        var test = TagToList.TryGetValue(tempTag, out oldTasks);
                        oldTasks.Add(tempTask);
                        TagToList.Remove(tempTag);
                        TagToList.Add(tempTag, oldTasks);
                    }
                    else
                    {
                        List<SingleTask> theTasks = new List<SingleTask>();
                        theTasks.Add(tempTask);
                        TagToList.Add(tempTag, theTasks);
                    }
                }

            }
            var count = 0;
            foreach (KeyValuePair<Tag, List<SingleTask>> lst in TagToList)
            {
                TaskList tempPls = new TaskList(lst.Value, count, lst.Key.Title);
                count++;
                projectTasks.Add(tempPls);
            }


            SingleProjectViewModel plsWork = new SingleProjectViewModel(tempViewProject, theMembers, projectTasks);

            return View(plsWork);
        }
        public Boolean userHasAccess(int requestedID)
        {
            var signedInUserIdentity = User.Identity.GetUserId();
            List<int> teamIDs = new List<int>();
            var allTeamMembers = db.TeamMembers.Where(tm => tm.MemberId.Equals(signedInUserIdentity)).ToList();
            for (int i = 0; i < allTeamMembers.Count; i++)
            {
                int curID = allTeamMembers[i].TeamId;
                teamIDs.Add(curID);
            }
            //will not have gotten here unless project exists
            Project proj = db.Projects.Find(requestedID);

            List<TeamProject> tps = new List<TeamProject>();
            foreach (int i in teamIDs)
            {
                List<TeamProject> tempTPs = db.TeamProjects.Where(tp => tp.ProjectId == proj.Id).ToList();
                tps.AddRange(tempTPs);
            }
            Boolean userHasAccess = false;

            foreach (TeamProject tp in tps)
            {
                foreach (int i in teamIDs)
                {
                    if (tp.TeamId == i)
                    {
                        userHasAccess = true;
                    }
                }
            }

            return userHasAccess;
        }

        //called when a user wants to change members
        public PartialViewResult ChangeMembers(int projid)
        {
            List<TeamProject> tempPeople = db.TeamProjects.Where(tp => tp.ProjectId == projid).ToList();
            List<AspNetUser> people = new List<AspNetUser>();
            foreach (TeamProject tp in tempPeople)
            {
                foreach (TeamMember t in tp.Team.TeamMembers)
                {
                    people.Add(db.AspNetUsers.Where(p => p.Id.Equals(t.MemberId)).FirstOrDefault());
                }
            }

            ViewBag.Members = people.Distinct();

            return PartialView("OpenEditMembers");
        }

        //called when they change members
        public ActionResult MemberDeleted(int memberid, int noteid)
        {
            //should prob assign them back to the model

            //will reload the note page to update the members, ignore the 0, it was for not getting yelled at
            return Index(0);
        }

        //called when a user clicks the button to change theme stuff
        public PartialViewResult ChangeTheme(int projid)
        {
            Project temp = db.Projects.Where(p => p.Id == projid).Single();
            ViewBag.theProject = temp;
            return PartialView("OpenEditTheme");
        }

        //called when a user edits theme preferences
        [HttpPost]
        public ActionResult ThemeEdited()
        {
            //gets the theme stuff
            string bgColor = Request["bgColor"];
            string fontcolor = Request["color"];
            string font = Request["font"];
            int projid = Convert.ToInt32(Request["projid"].ToString());
            //should prob reassign that stuff

            foreach (Project p in db.Projects.Where(p => p.Id == projid))
            {
                p.Background_Color = bgColor;
                p.Font_Color = fontcolor;
                p.Font_Url = font;
            }

            db.SaveChanges();

            //updates the note view to have the new stuff
            return Index(projid);
        }


        [HttpPost]
        public IEnumerable<AspNetUser> UserSearched()
        {
            //gets the username the user is looking for
            string searched = Request["username"];

            //pls return me the list of users that contain the string in their username

            List<AspNetUser> returnedUsers = db.AspNetUsers.Where(p => p.UserName.ToLower().Contains(searched.ToLower())).ToList();

            return returnedUsers;
        }

        //Opens the pop up to edit members
        public ActionResult OpenEditMembers(IEnumerable<AspNetUser> members)
        {
            ViewBag.Members = members;
            return View();
        }

        //opend the pop up to edit theme stuff
        public ActionResult OpenEditTheme(SingleTask task)
        {
            return View(task);
        }

        public PartialViewResult ViewTask(SingleTask task)
        {
            return PartialView(task);
        }

        [HttpPost]
        public ActionResult TaskEdited()
        {
            string title = Request["title"];
            string content = Request["content"];
            int taskid = Convert.ToInt32(Request["taskid"].ToString());
            string backgroundColor = Request["bgColor"];
            string color = Request["color"];
            string font = Request["font"];
            var members = Request["members"];

            //can make whatever out of that

            return null;
        }

        public ActionResult Archive(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            //Archive and Log Notes Related to the Project
            List<ProjectNote> projectNotes = db.ProjectNotes.Where(pn => pn.ProjectId == project.Id).ToList();
            List<Note> notes = db.Notes.ToList();
            foreach (Note note in notes)
            {
                foreach (ProjectNote projectNote in projectNotes)
                {
                    if (note.Id.Equals(projectNote.NoteId))
                    {
                        NoteArchive noteArchive = new NoteArchive { Title = note.Title, Content = note.Content, Date_Created = note.Date_Created, Date_Due = DateTime.Now.AddDays(30) };
                        db.NoteArchives.Add(noteArchive);
                        Log log = new Log { Date_Edited = DateTime.Now, Editor = User.Identity.GetUserId(), StoredId = "Z" + noteArchive.Id, Details = note.Id + " was and is now Z" + noteArchive.Id };
                        //Making NoteTags refer to the NoteArchive.StoredId
                        foreach (NoteTag tag in db.NoteTags.Where(t => t.StoredId == "N" + note.Id))
                        {
                            tag.StoredId = log.StoredId;
                        }

                        db.Logs.Add(log);
                        db.Notes.Remove(note);
                        db.ProjectNotes.Remove(projectNote);
                    }
                }
            }
            TeamProject tem = db.TeamProjects.Where(p => p.ProjectId == project.Id).FirstOrDefault();
            //Archive and Log Project
            ProjectArchive projectArchive = new ProjectArchive { Name = project.Name, Date_Created = project.Date_Created, Date_Due = DateTime.Now.AddDays(30), TeamId = tem.TeamId };
            db.ProjectArchives.Add(projectArchive);
            db.SaveChanges();
            int arciveId = db.ProjectArchives.OrderByDescending(p => p.Id).First().Id;
            string userId = User.Identity.GetUserId();
            
            db.Logs.Add(new Log { Date_Edited = DateTime.Now, Editor = User.Identity.GetUserId(), StoredId = "Q" + projectArchive.Id, Details = project.Id + " was and is now Q" + arciveId, TeamId = tem.TeamId });
            db.TeamProjects.Remove(tem);
            db.Projects.Remove(project);
            db.SaveChanges();

            ViewBag.Message = "Your project has been archived.";
            return View("RedirectPls");
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult RedirectPls()
        {
            ViewBag.Message = "Your project has been successfully archived.";
            return View();
        }

        [HttpPost, ActionName("Create")]
        public ActionResult ProjectCreated()
        {
            var title = Request["title"];
            var date = Request["datedue"];

            string pattern = "MM/dd/yyyy";
            DateTime DueDate;
            Project project = new Project();
            if (date != "")
            {
                DateTime.TryParseExact(date, pattern, null, DateTimeStyles.None, out DueDate);
                project.Name = title;
                project.Date_Created = DateTime.Now;
                project.Date_Due = DueDate;
            }
            else
            {
                project.Name = title;
                project.Date_Created = DateTime.Now;
            }

            db.Projects.Add(project);
            db.SaveChanges();
            int projectId = db.Projects.OrderByDescending(p => p.Id).First().Id;
            string userId = User.Identity.GetUserId();
            List<Team> soloTeams = db.Teams.Where(w => w.TeamMembers.Count == 1).ToList();
            int teamId = 0;
            foreach (Team solo in soloTeams)
            {
                foreach (TeamMember meme in db.TeamMembers)
                {
                    if (meme.MemberId == userId && meme.TeamId == solo.Id)
                    {
                        teamId = meme.TeamId;
                    }
                }
            }
            TeamProject tproject = new TeamProject { TeamId = teamId, ProjectId = projectId };
            db.TeamProjects.Add(tproject);
            db.SaveChanges();
            return RedirectToRoute("Default");
        }
    }
}