﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using project_tiki.Entities;

namespace project_tiki.Controllers
{
    public class ProjectArchivesController : Controller
    {
        private TikiEntities db = new TikiEntities();

        // GET: ProjectArchives
        public ActionResult Index()
        {
            return View(db.ProjectArchives.ToList());
        }

        // GET: ProjectArchives/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectArchive projectArchive = db.ProjectArchives.Find(id);
            if (projectArchive == null)
            {
                return HttpNotFound();
            }
            return View(projectArchive);
        }

        //GET: ProjectArchives/Unarchive/5
        public ActionResult Unarchive(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectArchive projectArchive = db.ProjectArchives.Find(id);
            if (projectArchive == null)
            {
                return HttpNotFound();
            }
            Project project = new Project { Name = projectArchive.Name, Date_Created = projectArchive.Date_Created };
            db.Projects.Add(project);
            Project projId = db.Projects.OrderByDescending(p => p.Id).First();
            TeamProject tp = new TeamProject { ProjectId = projId.Id, TeamId = projectArchive.TeamId.HasValue ? projectArchive.TeamId.Value : 0};
            List<Log> logs = db.Logs.Where(l => l.StoredId.StartsWith("Z") && l.TeamId == projectArchive.TeamId).ToList();
            List<NoteArchive> notes = db.NoteArchives.Where(n => n.TeamId == projectArchive.TeamId.Value).ToList();
            foreach (NoteArchive note in notes)
            {
                Note newNote = new Note { Title = note.Title, Content = note.Content, Date_Created = note.Date_Created };
                db.Notes.Add(newNote);
                Note noteId = db.Notes.OrderByDescending(p => p.Id).First();
                foreach (Log log in logs)
                {
                    foreach (NoteTag tag in db.NoteTags.Where(t => t.StoredId == log.StoredId))
                    {
                        tag.StoredId = "N" + noteId.Id;
                    }
                }

                ProjectNote pn = new ProjectNote { NoteId = noteId.Id, ProjectId = projId.Id };
                db.ProjectNotes.Add(pn);
                db.NoteArchives.Remove(note);
            }
            db.ProjectArchives.Remove(projectArchive);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: ProjectArchives/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectArchive projectArchive = db.ProjectArchives.Find(id);
            if (projectArchive == null)
            {
                return HttpNotFound();
            }
            return View(projectArchive);
        }

        // POST: ProjectArchives/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProjectArchive projectArchive = db.ProjectArchives.Find(id);
            db.ProjectArchives.Remove(projectArchive);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
