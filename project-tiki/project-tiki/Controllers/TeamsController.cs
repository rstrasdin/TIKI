﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using project_tiki.Entities;
using Microsoft.AspNet.Identity;

namespace project_tiki.Controllers
{
    public class TeamsController : Controller
    {
        private TikiEntities db = new TikiEntities();

        // GET: Teams
        public ActionResult Index()
        {
            var signedInUserIdentity = User.Identity.GetUserId();
            List<Team> teams = new List<Team>();
            var allTeamMembers = db.TeamMembers.Where(tm => tm.MemberId.Equals(signedInUserIdentity)).ToList();
            for(int i = 0; i < allTeamMembers.Count; i++)
            {
                int curID = allTeamMembers[i].TeamId;
                var theTeams = db.Teams.Where(t => t.Id == curID).ToList();
                for(int j = 0; j < theTeams.Count; j++)
                {
                    var tempTeam = theTeams[j];
                    teams.Add(tempTeam);
                }
            }

            
            //PLS I SWEAR TO GOD JUST MERGE OKAY PLSPLSPLSPLSPLS
            return View(teams);
        }

        // GET: Teams/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            if (!userHasAccess((int)id))
            {
                throw new UnauthorizedAccessException("Sorry, you do not have access to that team | We'll probably want to redirect them maybe?");
            }

            return View(team);
        }

        // GET: Teams/Create
        public ActionResult Create()
        {
            //ViewBag.Owner = new SelectList(db.AspNetUsers, "Id", "Email");
            
            return View();
        }

        // POST: Teams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Owner,Name")] Team team)
        {
            if (ModelState.IsValid)
            {
                db.Teams.Add(team);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Owner = new SelectList("Id", "Email", team.Owner);
            return View(team);
        }

        // GET: Teams/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            ViewBag.Owner = new SelectList("Id", "Email", team.Owner);
            return View(team);
        }

        // POST: Teams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Owner,Name")] Team team)
        {
            if (ModelState.IsValid)
            {
                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Owner = new SelectList("Id", "Email", team.Owner);
            return View(team);
        }

        // GET: Teams/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Team team = db.Teams.Find(id);
            db.Teams.Remove(team);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //
        public Boolean userHasAccess(int requestedID)
        {
            var signedInUserIdentity = User.Identity.GetUserId();
            List<int> teamIDs = new List<int>();
            var allTeamMembers = db.TeamMembers.Where(tm => tm.MemberId.Equals(signedInUserIdentity)).ToList();
            for (int i = 0; i < allTeamMembers.Count; i++)
            {
                int curID = allTeamMembers[i].TeamId;
                teamIDs.Add(curID);
            }

            if (teamIDs.Contains(requestedID))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
