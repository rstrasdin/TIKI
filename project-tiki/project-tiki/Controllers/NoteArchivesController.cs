﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using project_tiki.Entities;

namespace project_tiki.Controllers
{
    public class NoteArchivesController : Controller
    {
        private TikiEntities db = new TikiEntities();

        // GET: NoteArchives
        public ActionResult Index()
        {
            return View(db.NoteArchives.ToList());
        }

        // GET: NoteArchives/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoteArchive noteArchive = db.NoteArchives.Find(id);
            if (noteArchive == null)
            {
                return HttpNotFound();
            }
            return View(noteArchive);
        }

        public ActionResult Unarchive(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoteArchive noteArchive = db.NoteArchives.Find(id);
            if (noteArchive == null)
            {
                return HttpNotFound();
            }
            List<Log> logs = db.Logs.Where(l => l.StoredId.StartsWith("Z") && l.TeamId == noteArchive.TeamId).ToList();
            Note newNote = new Note { Title = noteArchive.Title, Content = noteArchive.Content, Date_Created = noteArchive.Date_Created };
            db.Notes.Add(newNote);
            Note noteId = db.Notes.OrderByDescending(p => p.Id).First();
            TeamNote tn = new TeamNote { TeamId = noteArchive.TeamId.Value , NoteId = noteId.Id };
            foreach (Log log in logs)
            {
                foreach (NoteTag tag in db.NoteTags.Where(t => t.StoredId == log.StoredId))
                {
                    tag.StoredId = "N" + noteId.Id;
                }
            }
            db.Notes.Add(newNote);
            db.TeamNotes.Add(tn);
            db.NoteArchives.Remove(noteArchive);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: NoteArchives/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NoteArchive noteArchive = db.NoteArchives.Find(id);
            if (noteArchive == null)
            {
                return HttpNotFound();
            }
            return View(noteArchive);
        }

        // POST: NoteArchives/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NoteArchive noteArchive = db.NoteArchives.Find(id);
            db.NoteArchives.Remove(noteArchive);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
