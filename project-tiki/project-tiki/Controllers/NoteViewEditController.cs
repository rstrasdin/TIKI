﻿using project_tiki.Entities;
using Microsoft.AspNet.Identity;
using project_tiki.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace project_tiki.Controllers
{
    public class NoteViewEditController : Controller
    {
        private TikiEntities db = new TikiEntities();
        // GET: NoteViewEdit
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            if (!userHasAccess((int)id))
            {
                return RedirectToAction("Index", "UserLanding");
            }

            //~~~~~~~~~~~~~~~
            
           
            //gets the first instance of note where ids match because the IDs should be unique
            Note theNote = db.Notes.Where(no => no.Id == (int)id).FirstOrDefault();
            //gets the teamID from the note
            int curTeamID = db.TeamNotes.Where(tn => tn.NoteId == (int)id).FirstOrDefault().TeamId;
            //gets all teamMembers on that team
            List<TeamMember> teamMembers = db.TeamMembers.Where(tm => tm.TeamId == curTeamID).ToList();
            List<AspNetUser> theMembers = new List<AspNetUser>();
            foreach (TeamMember tm in teamMembers)
            {
                var memID = tm.MemberId;
                //adds all AspNetUser counterparts to teamMembers
                theMembers.AddRange(db.AspNetUsers.Where(p => p.Id.Equals(memID)));
            }
            //gets the last time the note was put into the log table
            String tempStoredID = "N" + (int)id;
            var lastEdit = db.Logs.Where(l => l.StoredId.Equals(tempStoredID)).ToList();
            Note actualNote = db.Notes.Where(no => no.Id == (int)id).FirstOrDefault();
            DateTime lastEdited = actualNote.Date_Created;
            for (int j = 0; j < lastEdit.Count; j++)
            {
                if (j == lastEdit.Count - 1)
                {
                    lastEdited = lastEdit.ElementAt(j).Date_Edited;
                }
            }
            //gets all noteTags tied to current note
            List<NoteTag> noteTags = db.NoteTags.Where(no => no.StoredId.Equals(tempStoredID)).ToList();
            List<Tag> tags = new List<Tag>();
            foreach (NoteTag nt in noteTags)
            {
                //gets all tags corresponding to the tagIDs
                tags.AddRange(db.Tags.Where(t => t.Id == nt.TagId).ToList());
            }

            ViewNote tempNote2 = new ViewNote(theNote, theMembers, lastEdited, tags);


            NoteViewEditViewModel tempModel = new NoteViewEditViewModel(tempNote2, theMembers);
            return View(tempModel);
        }
    public Boolean userHasAccess(int requestedID)
        {
            var signedInUserIdentity = User.Identity.GetUserId();
            List<int> teamIDs = new List<int>();
            var allTeamMembers = db.TeamMembers.Where(tm => tm.MemberId.Equals(signedInUserIdentity)).ToList();
            for (int i = 0; i < allTeamMembers.Count; i++)
            {
                int curID = allTeamMembers[i].TeamId;
                teamIDs.Add(curID);
            }
            //will not have gotten here unless project exists
            Note note = db.Notes.Find(requestedID);
            List<TeamNote> allTNs = new List<TeamNote>();
            foreach(int t in teamIDs)
            {
                List<TeamNote> tns = db.TeamNotes.Where(tn => tn.TeamId == t).ToList();
                allTNs.AddRange(tns);
            }
            Boolean userHasAccess = false;

            foreach (TeamNote tn in allTNs)
            {
                foreach (int i in teamIDs)
                {
                    if (tn.TeamId == i)
                    {
                        userHasAccess = true;
                    }
                }
            }

            return userHasAccess;
        }

 //called when a user wants to change members
        public ViewResult ChangeMembers(int noteid)
        {
            int actualID = noteid;
            ViewBag.NoteID = actualID;
            List<AspNetUser> tempMembers = new List<AspNetUser>();
            List <TeamNote> teamNotes = db.TeamNotes.Where(tn => tn.NoteId == actualID).ToList();
            foreach(TeamNote tn in teamNotes)
            {
                List<TeamMember> tempTMs = db.TeamMembers.Where(tm => tm.TeamId == tn.TeamId).ToList();
                foreach(TeamMember tm in tempTMs)
                {
                    tempMembers.AddRange(db.AspNetUsers.Where(p => p.Id.Equals(tm.MemberId)).ToList());
                }
            }
            ViewBag.Members = tempMembers.Distinct();
            return View("OpenEditMembers");
        }

        //called when they change members
        public ActionResult MemberDeleted(int memberid, int noteid)
        {
            //should prob assign them back to the model

            //will reload the note page to update the members, ignore the 0, it was for not getting yelled at
            return Index(0);
        }

        //called when a user clicks the button to change theme stuff
        //public ViewResult ChangeTheme(int noteid) {
        //    ViewBag.theNote = db.Notes.Where(n => n.Id == noteid).FirstOrDefault();
        //    return View("OpenEditTheme");
        //}

        //called when a user edits theme preferences
        [HttpPost]
        public ActionResult ChangeTheme(){
            //gets the theme stuff
            var idefk = Request.Form;
            string bgColor = Request["bgColor"];
            string fontcolor = Request["color"];
            string font = Request["font"];
            int noteid = Convert.ToInt32(Request["noteid"].ToString());
            //should prob reassign that stuff

            foreach(Note n in db.Notes.Where(n => n.Id == noteid))
            {
                n.Background_Color = bgColor;
                n.Font_Color = fontcolor;
                n.Font_Url = font;
            }

            db.SaveChanges();

            //updates the note view to have the new stuff
            return RedirectToAction("Index", "UserLanding", null);
        }

        //gets the form information from the note view edit view
        [HttpPost]
        public ActionResult Edited()
        {
            
            string title = Request["title"];
            string content = Request["content"];
            string tags = Request["tags"];
            int noteid = Convert.ToInt32(Request["noteid"].ToString());
            string backgroundColor = Request["bgColor"];
            string color = Request["color"];
            string font = Request["font"];
            var members = Request["members"];
            Note tempNote = db.Notes.Where(no => no.Id == noteid).FirstOrDefault();
            Note n = new Note
            {
                Background_Color = backgroundColor,
                Content = content,
                Date_Created = tempNote.Date_Created,
                Font_Color = color,
                Font_Url = font,
                Title = title,
                Id = tempNote.Id,
                Date_Due = tempNote.Date_Due,
                ProjectNotes = tempNote.ProjectNotes,
                TeamNotes = tempNote.TeamNotes
            };

            List<String> tempTags = tags.Split(',').ToList();
            String storeID = "N" + noteid;
            foreach(String s in tempTags)
            {
                String tempStr = s.Trim(' ');
                
                List<Tag> posTag = db.Tags.Where(t => t.Title.ToLower().Equals(tempStr.ToLower())).ToList();
                if (posTag.Count > 0)
                {
                    List<NoteTag> nts = db.NoteTags.Where(nt => nt.StoredId.Equals(storeID)).ToList();
                    Boolean matchFound = false;
                    foreach(NoteTag nt in nts)
                    {
                        if(nt.TagId == posTag.First().Id)
                        {
                            matchFound = true;
                        }
                    }
                    if (!matchFound)
                    {
                        db.NoteTags.Add(new NoteTag { StoredId = storeID, TagId = posTag.First().Id });
                        db.SaveChanges();
                    }
                }else
                {
                    int tempTagIDOLD = db.Tags.OrderByDescending(t => t.Id).First().Id;
                    Tag theTempTag = new Tag { Title = tempStr , Id = (tempTagIDOLD + 1) };
                    db.Tags.Add(theTempTag);
                    db.SaveChanges();
                    int tempTagID = db.Tags.OrderByDescending(t => t.Id).First().Id;
                    db.NoteTags.Add(new NoteTag { StoredId = storeID, TagId = tempTagID });
                    db.SaveChanges();
                }
            }

            //REMOVE THE TAGS THAT AREN'T THERE

            List<NoteTag> allTheNoteTags = db.NoteTags.Where(nt => nt.StoredId == storeID).ToList();
            foreach(NoteTag nt in allTheNoteTags)
            {
                Tag curTag = nt.Tag;
                Boolean tagIsFound = false;
                foreach(String s in tempTags)
                {
                    if (s.Trim().ToLower().Equals(curTag.Title.Trim().ToLower()))
                    {
                        tagIsFound = true;
                    }
                }
                if (!tagIsFound)
                {
                    db.NoteTags.Remove(nt);
                    db.SaveChanges();
                }
            }







            foreach(Note tempNotePls in db.Notes.Where(no => no.Id == noteid))
            {
                tempNotePls.Background_Color = n.Background_Color;
                tempNotePls.Content = n.Content;
                tempNotePls.Date_Created = n.Date_Created;
                tempNotePls.Font_Color = n.Font_Color;
                tempNotePls.Font_Url = n.Font_Url;
                tempNotePls.Title = n.Title;
            }

            db.SaveChanges();
            return RedirectToRoute("UserLanding");
        }

        [HttpPost]
        public IEnumerable<AspNetUser> UserSearched()
        {
            //gets the username the user is looking for
            string searched = Request["username"];

            //pls return me the list of users that contain the string in their username

            List<AspNetUser> returnedUsers = db.AspNetUsers.Where(p => p.UserName.ToLower().Contains(searched.ToLower())).ToList();

            return returnedUsers;
        }

        //Opens the pop up to edit members
        public ActionResult OpenEditMembers(IEnumerable<AspNetUser> members)
        {
            ViewBag.Members = members;
            return View();
        }

        //opend the pop up to edit theme stuff
        public ActionResult OpenEditTheme(int? id)
       {
            Note note = db.Notes.Where(n => n.Id == id).FirstOrDefault();

            ViewNote tempNote = new ViewNote(note, null, System.DateTime.Now, null);
            return View(tempNote);
        }

        public ActionResult Archive(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            int teamId = db.TeamNotes.Where(w => w.NoteId == note.Id).First().TeamId;
            db.TeamNotes.RemoveRange(db.TeamNotes.Where(w => w.NoteId == note.Id));
            db.ProjectNotes.RemoveRange(db.ProjectNotes.Where(w => w.NoteId == note.Id));
            NoteArchive noteArchive = new NoteArchive { Title = note.Title, Content = note.Content, Date_Created = note.Date_Created, Date_Due = DateTime.Now.AddDays(30), TeamId = teamId };
            db.NoteArchives.Add(noteArchive);
            Log log = new Log { Date_Edited = DateTime.Now, Editor = User.Identity.GetUserId(), StoredId = "Z" + noteArchive.Id, Details = note.Id + " was and is now Z" + noteArchive.Id, TeamId = teamId };
            //Making NoteTags refer to the NoteArchive.StoredId
            foreach (NoteTag tag in db.NoteTags.Where(t => t.StoredId == "N" + note.Id))
            {
                tag.StoredId = log.StoredId;
            }

            db.Logs.Add(log);
            db.Notes.Remove(note);
            ViewBag.Message = "Your project has been archived.";
            return View("RedirectPls");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost, ActionName("Create")] 
        public ActionResult NoteCreated()
        {
            var title = Request["title"];
            var content = Request["content"];
            var tags = Request["tags"];

            Note newNote = new Note { Title = title, Content = content, Date_Created = DateTime.Now };
            db.Notes.Add(newNote);
            db.SaveChanges();
            string userId = User.Identity.GetUserId();
            List<Team> soloTeams = db.Teams.Where(w => w.TeamMembers.Count == 1).ToList();
            int teamId = 0;
            foreach(Team solo in soloTeams)
            {
                foreach(TeamMember meme in db.TeamMembers)
                {
                    if(meme.MemberId == userId && meme.TeamId == solo.Id)
                    {
                        teamId = meme.TeamId;
                    }
                }
            }

            int noteid= db.Notes.OrderByDescending(p => p.Id).First().Id;
            TeamNote tnote = new TeamNote { NoteId = noteid, TeamId = teamId };
            db.TeamNotes.Add(tnote);
            List<String> tempTags = tags.Split(',').ToList();
            String storeID = "N" + noteid;
            foreach (String s in tempTags)
            {
                String tempStr = s.Trim(' ');

                List<Tag> posTag = db.Tags.Where(t => t.Title.ToLower().Equals(tempStr.ToLower())).ToList();
                if (posTag.Count > 0)
                {
                    List<NoteTag> nts = db.NoteTags.Where(nt => nt.StoredId.Equals(storeID)).ToList();
                    Boolean matchFound = false;
                    foreach (NoteTag nt in nts)
                    {
                        if (nt.TagId == posTag.First().Id)
                        {
                            matchFound = true;
                        }
                    }
                    if (!matchFound)
                    {
                        db.NoteTags.Add(new NoteTag { StoredId = storeID, TagId = posTag.First().Id });
                    }
                }
                else
                {
                    int tempTagIDOLD = db.Tags.OrderByDescending(t => t.Id).First().Id;
                    Tag theTempTag = new Tag { Title = tempStr, Id = (tempTagIDOLD + 1) };
                    db.Tags.Add(theTempTag);
                    int tempTagID = db.Tags.OrderByDescending(t => t.Id).First().Id;
                    db.NoteTags.Add(new NoteTag { StoredId = storeID, TagId = tempTagID });     
                }
            }
            db.SaveChanges();
            return RedirectToRoute("Default");
        }
        
        public void AddSingleMember(AspNetUser newMember, int noteID)
        {
            TeamNote theirTeam = db.TeamNotes.Where(tn => tn.NoteId == noteID).FirstOrDefault();
            TeamMember tm = new TeamMember { MemberId = newMember.Id, TeamId = theirTeam.Id };
            db.TeamMembers.Add(tm);
            db.SaveChanges();
        }
    }
}