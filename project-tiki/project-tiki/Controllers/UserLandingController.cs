﻿using Microsoft.AspNet.Identity;
using project_tiki.Entities;
using project_tiki.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project_tiki.Controllers
{
    public class UserLandingController : Controller
    {
        private TikiEntities db = new TikiEntities();
        // GET: UserLanding
        public ActionResult Index()
        {
            var signedInUserIdentity = User.Identity.GetUserId();
            UserLandingViewModel tempModel = null;
            //~~~~~~~~~~~~~~Projects~~~~~~~~~~~~~~
            //gets all of the teams that they are in
            var userTeams = db.TeamMembers.Where(t => t.MemberId.Equals(signedInUserIdentity)).ToList();
            List<int> userTeamIDs = new List<int>();
            //pulls out all of the team ids of the teams the person is in
            for(int i = 0; i < userTeams.Count; i++)
            {
                userTeamIDs.Add(userTeams.ElementAt(i).TeamId);
            }
            List<TeamNote> allNotes = new List<TeamNote>();
            //gets all notes that the user has access to.
            foreach(int t in userTeamIDs)
            {
                allNotes.AddRange(db.TeamNotes.Where(tn => tn.TeamId == t).ToList());
            }
            List<int> noteIDs = new List<int>();
            //gets all of the noteIDs
            foreach(TeamNote tn in allNotes)
            {
                noteIDs.Add(tn.NoteId);
            }
            List<ViewNote> finalNotes = new List<ViewNote>();
            foreach(int n in noteIDs)
            {
                //gets the first instance of note where ids match because the IDs should be unique
                Note theNote = db.Notes.Where(no => no.Id == n).FirstOrDefault();
                //gets the teamID from the note
                int curTeamID = db.TeamNotes.Where(tn => tn.NoteId == n).FirstOrDefault().TeamId;
                //gets all teamMembers on that team
                List<TeamMember> teamMembers = db.TeamMembers.Where(tm => tm.TeamId == curTeamID).ToList();
                List<AspNetUser> theMembers = new List<AspNetUser>();
                foreach(TeamMember tm in teamMembers)
                {
                    var memID = tm.MemberId;
                    //adds all AspNetUser counterparts to teamMembers
                    theMembers.AddRange(db.AspNetUsers.Where(p => p.Id.Equals(memID)));
                }
                //gets the last time the note was put into the log table
                String tempStoredID = "N" + n;
                var lastEdit = db.Logs.Where(l => l.StoredId.Equals(tempStoredID)).ToList();
                Note actualNote = db.Notes.Where(no => no.Id == n).FirstOrDefault();
                DateTime lastEdited = actualNote.Date_Created;
                for(int j = 0; j < lastEdit.Count; j++)
                {
                    if( j == lastEdit.Count - 1)
                    {
                        lastEdited = lastEdit.ElementAt(j).Date_Edited;
                    }
                }
                //gets all noteTags tied to current note
                List<NoteTag> noteTags = db.NoteTags.Where(no => no.StoredId.Equals(tempStoredID)).ToList();
                List<Tag> tags = new List<Tag>();
                foreach (NoteTag nt in noteTags)
                {
                    //gets all tags corresponding to the tagIDs
                    tags.AddRange(db.Tags.Where(t => t.Id == nt.TagId).ToList());
                }

                ViewNote tempNote = new ViewNote(theNote, theMembers, lastEdited, tags);
                finalNotes.Add(tempNote);

            }

            //~~~~~~~~~~~~~~Projects~~~~~~~~~~~~~~

            List<ViewProject> finalProjects = new List<ViewProject>();
            tempModel = new UserLandingViewModel(finalProjects, finalNotes);
            List<TeamProject> teamProjects = new List<TeamProject>();
            foreach(int t in userTeamIDs)
            {
                //add all teamProjects
                teamProjects.AddRange(db.TeamProjects.Where(tp => tp.TeamId == t));
            }
            List<Project> actualProjects = new List<Project>();
            foreach(TeamProject tp in teamProjects)
            {
                //extracts all the projects from teamProjects
                actualProjects.Add(tp.Project);
            }
            //public ViewProject(Project baseProject, List<AspNetUser> members, DateTime lastEdited)
            foreach (Project proj in actualProjects)
            {
                int curTeamID = db.TeamProjects.Where(tp => tp.ProjectId == proj.Id).FirstOrDefault().TeamId;
                //gets all teamMembers on that team
                List<TeamMember> teamMembers = db.TeamMembers.Where(tm => tm.TeamId == curTeamID).ToList();
                List<AspNetUser> theMembers = new List<AspNetUser>();
                foreach (TeamMember tm in teamMembers)
                {
                    var memID = tm.MemberId;
                    //adds all AspNetUser counterparts to teamMembers
                    theMembers.AddRange(db.AspNetUsers.Where(p => p.Id.Equals(memID)));
                }
                DateTime lastEdited = proj.Date_Created;
                String tempStoredID = "P" + proj.Id;
                var lastEdit = db.Logs.Where(l => l.StoredId.Equals(tempStoredID)).ToList();
                for (int j = 0; j < lastEdit.Count; j++)
                {
                    if (j == lastEdit.Count - 1)
                    {
                        lastEdited = lastEdit.ElementAt(j).Date_Edited;
                    }
                }
                ViewProject tempViewProject = new ViewProject(proj, theMembers, lastEdited);
                finalProjects.Add(tempViewProject);
            }

            return View(tempModel);
        }

        public ActionResult DeleteProject(int id)
        {
            //delete project

            //refresh page
            return Index();
        }

        public ActionResult DeleteNote(int id)
        {
            //delete note

            //refresh page
            return Index();
        }
    }
}