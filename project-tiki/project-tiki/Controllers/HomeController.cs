﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project_tiki.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "UserLanding");
            }
            return View();
        }

        public ActionResult AboutProjects()
        {
            return View();
        }

        public ActionResult AboutNotes()
        {
            return View();
        }
    }
}