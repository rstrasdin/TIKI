﻿using Microsoft.AspNet.Identity;
using project_tiki.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace project_tiki
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "UserLanding",
                url: "UserLanding",
                defaults: new { controller = "UserLanding", action = "Index" }
                );


            routes.MapRoute(
                null,
                url: "CreateProject",
                defaults: new { controller = "SingleProject", action = "Create" }
                );

            routes.MapRoute(
                name: null,
                url: "SingleProject/Create",
                defaults: new { controller = "UserLanding", action = "Index" }
                );

            routes.MapRoute(
                name: null,
                url: "NoteViewEdit/Create",
                defaults: new { controller = "UserLanding", action = "Index" }
                );


            

            

            routes.MapRoute(
                null,
                url: "CreateNote",
                defaults: new { controller = "NoteViewEdit", action = "Create" }
                );

            routes.MapRoute(
                null,
                url: "NoteViewEdit/Edited",
                defaults: new { controller = "NoteViewEdit", action = "Edited" }
                );

            routes.MapRoute(
                null,
                url: "SingleProject/ViewTask/{id}",
                defaults: new { controller = "SingleProject", action = "ViewTask" }
                );

            routes.MapRoute(
                null,
                url: "SingleProject/{id}",
                defaults: new { controller = "SingleProject", action = "Index" }
                );


            routes.MapRoute(
                null,
                url: "SingleProject/ChangeMembers/{projid}",
                defaults: new { controller = "SingleProject", action = "ChangeMembers" }
                );

            routes.MapRoute(
                null,
                url: "SingleProject/ChangeTheme/{projid}",
                defaults: new { controller = "SingleProject", action = "ChangeTheme" }
                );

            


            routes.MapRoute(
                null,
                url: "Notes/{id}",
                defaults: new { controller = "NoteViewEdit", action = "Index", id = UrlParameter.Optional }
                );
            routes.MapRoute(
               null,
               url: "NoteArchive/",
               defaults: new
               {
                   controller = "NoteArchives",
                   action = "Index",
                   id = UrlParameter.Optional
               }
               );
            routes.MapRoute(
                null,
                url: "NoteArchive/{action}/{id}",
                defaults: new
                {
                    controller = "NoteArchives",
                    action = UrlParameter.Optional,
                    id = UrlParameter.Optional
                }
                );
            routes.MapRoute(
               null,
               url: "ProjectArchive/",
               defaults: new
               {
                   controller = "ProjectArchives",
                   action = "Index",
                   id = UrlParameter.Optional
               }
               );
            routes.MapRoute(
                null,
                url: "ProjectArchive/{action}/{id}",
                defaults: new
                {
                    controller = "ProjectArchives",
                    action = UrlParameter.Optional,
                    id = UrlParameter.Optional
                }
                );
            routes.MapRoute(
                null,
                url: "AboutProjects",
                defaults: new { controller = "Home", action = "AboutProjects" }
                );

            routes.MapRoute(
                null,
                url: "AboutNotes",
                defaults: new { controller = "Home", action = "AboutNotes" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CatchAll",
                url: "{*any}",
                defaults: new { controller = "Home", action = "Index" }
            );


        }
    }
}
